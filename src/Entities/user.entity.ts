import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Role } from './role.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 80, nullable: false, unique: true })
  email: string;

  @Column({ length: 50 })
  firstName: string;

  @Column({ length: 50 })
  lastName: string;

  @Column({ default: true })
  onLine: boolean;

  @Column({ length: 512 })
  authToken: string;

  @Column({ length: 512 })
  refreshToken: string;

  @Column({ length: 4096 })
  accessToken: string;

  @OneToOne(() => Role)
  @JoinColumn()
  role: Role;
}
