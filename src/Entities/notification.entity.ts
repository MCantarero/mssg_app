import { Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { Message } from './message.entity';

@Entity()
export class Notification {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Message)
  @JoinColumn()
  message: Message;

  @OneToOne(() => User)
  @JoinColumn()
  notificationSender: User;

  @OneToOne(() => User)
  @JoinColumn()
  notificationReceiver: User;
}
