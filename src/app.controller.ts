import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

//El controller es el manejador de rutas, es decir es el que define los endpoints, y que cuando son llamados, que funcion o funciones del service es llamada.

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
}
