import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { ApiResponseModule } from './modules/default/api.response.module';
import { UserModule } from './modules/User/user.module';

import { User } from './Entities/user.entity';
import { Message } from './Entities/message.entity';
import { Role } from './Entities/role.entity';
import { Notification } from './Entities/notification.entity';
import { RoleModule } from './modules/Role/role.module';

@Module({
  imports: [
    ConfigModule.forRoot({}),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'postgres@123.',
      database: 'kubide',
      entities: [User, Message, Role, Notification],
      synchronize: true,
    }),
    ApiResponseModule,
    UserModule,
    RoleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
