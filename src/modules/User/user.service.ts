import { Injectable } from '@nestjs/common';
import { User } from 'src/Entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private repository: Repository<User>,
  ) {}

  getUserData(id: number): Promise<User> {
    return this.repository.findOneBy({ id });
  }

  async addUser(body: any) {
    const newUser = this.repository.create(body);
    const alreadyExists = await this.isNewUser(body.email);
    if (!alreadyExists) {
      this.repository.save(newUser);
    }
  }

  async isNewUser(email: string) {
    const exists = await this.repository.countBy({ email });
    return exists;
  }
}
