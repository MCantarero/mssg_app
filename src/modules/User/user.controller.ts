import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { UserService } from './user.service';

//Aqui van los endpoints del user

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('getData:id')
  getUserData(@Param('id') id: number) {
    return this.userService.getUserData(id);
  }

  @Post('create')
  addUser(@Body() body: any) {
    return this.userService.addUser(body);
  }
}
