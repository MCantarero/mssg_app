import { Controller, Get, Req } from '@nestjs/common';
import { Request } from 'express';
import { ApiResponseService } from './api.response.service';

@Controller()
export class ApiResponseController {
  constructor(private readonly apiResponseService: ApiResponseService) {}
  @Get('/getApiState')
  checkApiState(): string {
    return this.apiResponseService.checkApiState();
  }
}
