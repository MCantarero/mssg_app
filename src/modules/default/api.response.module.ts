import { Module } from '@nestjs/common';
import { ApiResponseController } from './api.response.controller';
import { ApiResponseService } from './api.response.service';

@Module({
  imports: [],
  providers: [ApiResponseService],
  controllers: [ApiResponseController],
})
export class ApiResponseModule {}
