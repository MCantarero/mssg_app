import { Injectable } from '@nestjs/common';

@Injectable()
export class ApiResponseService {
  checkApiState(): string {
    return 'API on-line';
  }
}
