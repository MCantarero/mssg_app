import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { RoleService } from './role.service';

//Aqui van los endpoints del user

@Controller('role')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Get('getData:id')
  getRoleData(@Param('id') id: number) {
    return this.roleService.getRoleData(id);
  }

  @Post('/create')
  addRole(@Body() body: any) {
    return this.roleService.addRole(body);
  }
  @Get('/getRoles')
  getRoles() {
    return this.roleService.getAll();
  }
}
