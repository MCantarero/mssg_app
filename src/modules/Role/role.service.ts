import { Injectable } from '@nestjs/common';
import { Role } from 'src/Entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role)
    private repository: Repository<Role>,
  ) {}

  getRoleData(id: number): Promise<Role> {
    return this.repository.findOneBy({ id });
  }

  async addRole(body: any) {
    const newRole = this.repository.create(body);
    const alreadyExists = await this.isNewRole(body.roleName);

    if (!alreadyExists) {
      this.repository.save(newRole);
    }
  }
  getAll() {
    return this.repository.find();
  }

  async isNewRole(roleName: string) {
    const exists = await this.repository.countBy({ roleName });
    return exists;
  }
}
