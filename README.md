## Description

 Messaging application using Node, NestJS and Docker.

## Development tools 

- Node --> My Version: v16.16.0
- NestJS --> My Version: 9.1.4

## Installation

Node installation
```bash
$ npm install
```
NestJS installation
```bash
$ npm i -g @nestjs/cli
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - Miguel Ángel Cantarero Ortega


## Kanban board

Join link: https://trello.com/invite/kubidemssg_app/ATTIf379a31571b84c4ce1cfc041ff58ea43DB01E1E7

